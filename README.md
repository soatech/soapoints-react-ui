# Overview #

Web UI for the SoaPoints API (https://bitbucket.org/soatech/soapoints-mean-api) done in ReactJS, currently in the style of ES5.

# Installation #

```
#!bash

$ npm install
```

# Build #

This will run watchify with reactify and eslintify.

```
#!bash

$ npm start
```

# Run Server #

For quick and easy local hosting.  This defaults to port 8080, but if it's taken it will keep going until it finds an open port.  It will print to the screen what port it ended up using.

```
#!bash

$ npm run server
```

This is built to talk to the SoaPoints API, which can be found here: https://bitbucket.org/soatech/soapoints-mean-api
'use strict';

import {TasksInitialState} from '../core/InitialState';
import {TASK_ACTIONS} from '../enums/Actions';
import assign from 'object-assign';

export default (state, action) => {
    let newState = assign({}, state);

    switch (action.type) {
        case TASK_ACTIONS.LIST_SHARED_TASKS:
        case TASK_ACTIONS.LIST_USER_TASKS:
            newState = assign(newState, {
                frequencyType: action.frequencyType,
                tasks: action.tasks
            });
            return newState;
        case TASK_ACTIONS.EDIT:
        case TASK_ACTIONS.VIEW:
            newState.selectedTask = action.task;
            return newState;
        default:
            return state || TasksInitialState;
    }
};
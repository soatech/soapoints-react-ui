'use strict';

export default {
    LOGIN: 'login',
    REGISTER: 'register',
    TASKS: 'tasks'
};
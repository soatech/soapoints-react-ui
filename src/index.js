'use strict';

//import './index.less';

import React from 'react';
import {render } from 'react-dom';
import SoaPointsApp from './core/SoaPointsApp';

render(<SoaPointsApp/>, document.getElementById('soapoints-react'));


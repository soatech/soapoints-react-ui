'use strict';

import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import ErrorMessageContainer from './ErrorMessageContainer';

const MultiMessageContainer = React.createClass({
    render: function render() {
        console.log("MMC Render");
        console.log(this.props);

        let errorCounter = 0;
        let errorKey;
        return <div className="multi-message-container">
            {_.map(this.props.messages.messages, (message) => {
                errorCounter += 1;
                errorKey = `error-${errorCounter}`;
                return <ErrorMessageContainer key={errorKey} message={message}/>;
            })}
        </div>;
    }
});

export default connect((state) => ({
    messages: state.messages
}))(MultiMessageContainer);
'use strict';

var React = require('react');
var BaseMessageContainer = require('BaseMessageContainer');

module.exports = React.createClass({
    render: function render() {
        return <BaseMessageContainer className="info" message={this.props.message}/>;
    }
});
'use strict';

var React = require('react');

module.exports = React.createClass({
    render: function render() {
        return <div className={this.props.className}>
            <span className="message">{this.props.message}</span>
        </div>;
    }
});
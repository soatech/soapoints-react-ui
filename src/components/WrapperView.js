'use strict';

import React from 'react';
import MultiMessageContainer from './messages/MultiMessageContainer';
import Routes from '../enums/Routes';
import {gotoRoute} from '../actions/RouteActions';
import {connect} from 'react-redux';

const WrapperView = React.createClass({
    componentWillMount() {
        // check to see if we are logged in.  if not, redirect to login
        if (!this.props.auth || !this.props.auth.authenticatedUser) {
            gotoRoute(Routes.LOGIN);
        }
    },
    render() {
        // can put navigation elements here
        return <div className="container">
            <MultiMessageContainer/>
            {this.props.children}
        </div>;
    }
});

export default connect((state) => ({auth: state.auth}))(WrapperView);

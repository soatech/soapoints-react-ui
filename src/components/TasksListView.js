'use strict';

import _ from 'lodash';
import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {listSharedTasks} from '../actions/TaskActions';

const TasksListView = React.createClass({
    componentDidMount: function componentDidMount() {
        console.log("TasksListView");
        console.log("componentDidMount");
        // fetch tasks from server
        this.props.dispatch(listSharedTasks({
            frequencyType: this.props.tasks.frequencyType,
            authToken: this.props.auth.authenticatedUser.token
        }));
    },
    render: function render() {
        console.log("TasksListView");
        console.log("render");
        console.log(this.props.tasks.tasks);
        return <div className="task-list">
            <div><span classID="so-bold">Shared Tasks:</span></div>
            <br/>
            {_.map(this.props.tasks.tasks, function (task) {
                return <div key={task._id} className="task-item"><Link to={`/task/${task._id}`}>{task.name}</Link>
                </div>;
            })}
        </div>;
    }
});

export default connect((state) => ({
    auth: state.auth,
    tasks: state.tasks
}))(TasksListView);

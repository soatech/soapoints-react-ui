'use strict';

import {Link} from 'react-router';
import React from 'react';
import {connect} from 'react-redux';
import {login} from '../actions/AuthActions';

const LoginView = React.createClass({
    _formOnSubmitHandler: function _formOnSubmitHandler(event) {
        event.preventDefault();

        this.props.dispatch(login({
            username: event.target.username.value,
            password: event.target.password.value
        }));
    },
    render: function render() {
        console.log("LoginView Render");
        console.log(this.props);

        return <div>
            <form onSubmit={this._formOnSubmitHandler}>
                <div className="form-group">
                    <label htmlFor="usernameInput">Username:</label>
                    <input type="text" placeholder="Username" id="usernameInput" name="username"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="passwordInput">Password:</label>
                    <input type="password" placeholder="************" id="passwordInput" name="password"
                           className="form-control"/>
                </div>
                <button type="submit" className="btn btn-default">Login</button>
            </form>

            <div>
                Need an account? <Link to="/register">Click Here</Link> to register!
            </div>
        </div>;
    }
});

export default connect()(LoginView);

'use strict';

import {Link} from 'react-router';
import React from 'react';
import {connect} from 'react-redux';
import {register} from '../actions/AuthActions';

const RegisterView = React.createClass({
    _formOnSubmitHandler(event) {
        event.preventDefault();

        this.props.dispatch(register({
            firstName: event.target.firstName.value,
            lastName: event.target.lastName.value,
            email: event.target.email.value,
            username: event.target.username.value,
            password: event.target.password.value
        }));
    },
    render() {
        return <div>
            <form onSubmit={this._formOnSubmitHandler}>
                <div className="form-group">
                    <label htmlFor="firstNameInput">First Name:</label>
                    <input type="text" placeholder="First Name"
                           id="firstNameInput" name="firstName"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="lastNameInput">Last Name:</label>
                    <input type="text" placeholder="Last Name"
                           id="lastNameInput" name="lastName"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="emailInput">Email:</label>
                    <input type="text" placeholder="me@example.com"
                           id="emailInput" name="email"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="usernameInput">Username:</label>
                    <input type="text" placeholder="Username"
                           id="usernameInput" name="username"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="passwordInput">Password:</label>
                    <input type="password" placeholder="************"
                           id="passwordInput" name="password"
                           className="form-control"/>
                </div>
                <button type="submit" className="btn btn-primary">Register</button>
                <Link to="/login">
                    <button type="button"
                            className="btn btn-default">Cancel
                    </button>
                </Link>
            </form>
        </div>;
    }
});

export default connect()(RegisterView);

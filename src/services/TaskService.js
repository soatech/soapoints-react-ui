'use strict';

import HttpMethods from '../enums/HttpMethods';
import assign from 'object-assign';
import baseServiceFactory from '../services/BaseService';
import FrequencyTypes from '../enums/FrequencyTypes';

const taskServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);

    return assign(baseService, {
        listSharedTasks: function listSharedTasks(frequencyType:String, success, failure) {
            let append = '/sharedtasks';


            // TODO: Verify this is legit option first
            // compare against enum set
            if (frequencyType && frequencyType.length) {
                append += `/${frequencyType}`;
            }

            if (FrequencyTypes.contains(frequencyType)) {
                console.log("CONTAINS!");
            }

            return fetch(this.buildUri(append), this.buildOptions(HttpMethods.GET))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        failure(responseData);
                    } else {
                        success(responseData);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    failure(error)
                });
        }
    });
};

export default taskServiceFactory;
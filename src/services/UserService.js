'use strict';

import HttpMethods from '../enums/HttpMethods';
import assign from 'object-assign';
import baseServiceFactory from '../services/BaseService';

const userServiceFactory = function (spec) {
    const baseService = baseServiceFactory(spec);

    return assign(baseService, {
        login: function login(user, success, failure) {

            let reqBody = {
                username: user.username,
                password: user.password
            };

            return fetch(this.buildUri('/auth/login'), this.buildOptions(HttpMethods.POST, reqBody))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        failure(responseData);
                    } else {
                        success(responseData);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    failure(error)
                });
        },
        register(user, success, failure) {
            let reqBody = {
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                username: user.username,
                password: user.password
            };

            return fetch(this.buildUri('/auth/register'), this.buildOptions(HttpMethods.POST, reqBody))
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseData.error) {
                        failure(responseData);
                    } else {
                        success(responseData);
                    }
                })
                .catch((error) => failure(error));
        }
    });
};

export default userServiceFactory;
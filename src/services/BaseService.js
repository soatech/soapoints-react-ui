'use strict';

import config from '../config/config';

const baseServiceFactory = function (spec) {
    const baseUrl = config.SERVER_BASE_URL;
    let authToken = spec.authToken;
    let baseOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    let BaseService = {
        getAuthToken: function getAuthToken() {
            return authToken;
        },
        setAuthToken: function setAuthToken(newToken:String) {
            authToken = newToken;

            if (authToken && authToken.length) {
                baseOptions.headers['Authorization'] = `JWT ${authToken}`;
            }
        },
        buildUri: function buildUri(append:String) {
            return baseUrl + append;
        },
        buildOptions: function buildOptions(method:String, reqBody:Object) {
            let options = baseOptions;

            options.method = method;
            options.body = JSON.stringify(reqBody);

            return options;
        }
    };

    BaseService.setAuthToken(authToken);

    return BaseService;
};

export default baseServiceFactory;
'use strict';

import React from 'react';
import {Route, IndexRoute} from 'react-router';
import WrapperView from '../components/WrapperView';
import LoginView from '../components/LoginView';
import RegisterView from '../components/RegisterView';
import TasksListView from '../components/TasksListView';

export default (
    <Route path="/" component={WrapperView}>
        <IndexRoute component={LoginView}/>
        <Route path="login" component={LoginView}/>
        <Route path="register" component={RegisterView}/>
        <Route path="tasks" component={TasksListView}/>
        <Route path="*" component={LoginView}/>
    </Route>
);

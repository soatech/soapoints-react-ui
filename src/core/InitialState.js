'use strict';

import FrequencyTypes from '../enums/FrequencyTypes';

export const MessagesInitialState = {
    messages: []
};

export const AuthInitialState = {
    authenticatedUser: undefined
};

export const TasksInitialState = {
    frequencyType: FrequencyTypes.DAILY,
    tasks: [],
    selectedTask: undefined
};

export default {
    auth: AuthInitialState,
    tasks: TasksInitialState,
    messages: MessagesInitialState
};
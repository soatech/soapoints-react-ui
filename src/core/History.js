'use strict';

/** TODO: eventually we need to create a server to support proper browser history
 * https://github.com/rackt/react-router/blob/master/docs/guides/basics/Histories.md
 */

import createHashHistory from 'history/lib/createHashHistory';

export default createHashHistory();
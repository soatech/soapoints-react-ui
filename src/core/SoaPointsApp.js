'use strict';

import React from 'react';
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import appStore from './AppStore';
import routes from './Routes';
import History from './History';

// React components for Redux DevTools
import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react';

const SoaPointsApp = React.createClass({
    render: function render() {
        return <div>
            <Provider store={appStore}>
                <Router history={History} routes={routes}/>
            </Provider>
            <DebugPanel top right bottom>
                <DevTools store={appStore} monitor={LogMonitor}/>
            </DebugPanel>
        </div>;
    }
});

export default SoaPointsApp;

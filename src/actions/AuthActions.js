'use strict';

import {AUTH_ACTIONS} from '../enums/Actions';
import Routes from '../enums/Routes';
import userServiceFactory from '../services/UserService';
import {addSystemMessage, clearSystemMessages} from './MessageActions';
import {startLoading, endLoading} from './SystemActions';
import {gotoRoute} from './RouteActions';
import History from '../core/History';

function loginSuccess(user) {
    return {
        type: AUTH_ACTIONS.LOGIN_SUCCESS,
        user
    };
}

function registerSuccess(user) {
    return {
        type: AUTH_ACTIONS.REGISTER_SUCCESS,
        user
    }
}

export function login(user) {
    const UserService = userServiceFactory({});

    return (dispatch) => {
        // clear any error messages we might have
        dispatch(clearSystemMessages());
        dispatch(startLoading());

        return UserService.login(user, (responseData) => {
                dispatch(loginSuccess(responseData));
                dispatch(endLoading());

                dispatch(gotoRoute(Routes.TASKS));
            },
            (errorObj) => {
                dispatch(addSystemMessage(errorObj));
                dispatch(endLoading());
            });
    };
}

export function register(user) {
    const UserService = userServiceFactory({});

    return (dispatch) => {
        dispatch(clearSystemMessages());
        dispatch(startLoading());

        return UserService.register(user, (responseData) => {
                dispatch(registerSuccess(responseData));
                dispatch(endLoading());

                dispatch(gotoRoute(Routes.TASKS));
            },
            (erroObj) => {
                console.log(erroObj);

                dispatch(addSystemMessage(erroObj));
                dispatch(endLoading());
            });
    };
}
'use strict';

import {SYSTEM_ACTIONS} from '../enums/Actions';

export function startLoading() {
    return {
        type: SYSTEM_ACTIONS.START_LOADING
    }
}

export function endLoading() {
    return {
        type: SYSTEM_ACTIONS.END_LOADING
    }
}
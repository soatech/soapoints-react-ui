'use strict';

import Routes from '../enums/Routes';
import History from '../core/History';

export function gotoRoute(route) {
    History.push(`/${route}`);

    return {
        type: "ROUTING"
    }
}

'use strict';

import {TASK_ACTIONS} from '../enums/Actions';
import taskServiceFactory from '../services/TaskService';
import {addSystemMessage, clearSystemMessages} from './MessageActions';

function listSharedTasksSuccess(spec) {
    console.log("listSharedTasksSuccess");
    console.log(spec);
    return {
        type: TASK_ACTIONS.LIST_SHARED_TASKS,
        tasks: spec.tasks,
        frequencyType: spec.frequencyType
    };
}

export function listSharedTasks(spec) {
    const TaskService = taskServiceFactory(spec);

    return (dispatch) => {
        // clear any error messages we might have
        dispatch(clearSystemMessages());

        return TaskService.listSharedTasks(spec.frequencyType, (responseData) => {
                dispatch(listSharedTasksSuccess({
                    tasks: responseData,
                    frequencyType: spec.frequencyType
                }));
            },
            (errorObj) => {
                dispatch(addSystemMessage(errorObj));
            });
    };
}
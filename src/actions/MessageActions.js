'use strict';

import {MESSAGE_ACTIONS} from '../enums/Actions';

export function addSystemMessage(messageRaw) {
    let message = {};

    if (typeof messageRaw !== Object) {
        message.error = messageRaw
    } else {
        message = messageRaw;
    }

    return {
        type: MESSAGE_ACTIONS.ADD,
        message
    };
}

export function clearSystemMessages() {
    return {
        type: MESSAGE_ACTIONS.CLEAR
    };
}